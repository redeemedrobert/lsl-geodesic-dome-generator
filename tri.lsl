//This script will go into a primite in the inventory of the main object. Upon being rendered, the object will listen for a message from the main object which will contain its final position, size, and rotation, and it will then set its parameters to those.

integer ln;
default
{
    on_rez(integer s){
        ln = llListen(s+777, "", "", "");
    }
    
    state_entry(){
        llListen(0, "", llGetOwner(), "derez");
    }
    listen(integer c, string n, key a, string m){
        if(c == 0) llDie();
        if((vector)llGetSubString(m, 0, llSubStringIndex(m, "|")-1)!=ZERO_VECTOR){
            vector scale = (vector)llGetSubString(m, 0, llSubStringIndex(m, "|") - 1);
            float shear = (float)llGetSubString(m, llSubStringIndex(m, "|")+1, -1);
            llSetPrimitiveParams([
            PRIM_TYPE, PRIM_TYPE_BOX, 0, <0,1,0>, 0, ZERO_VECTOR, <1,0,0>, <0, -shear, 0>,
            PRIM_SIZE, scale
        ]);
            llListenRemove(ln);
            llRemoveInventory("GeoTri");
        }
    }
}
