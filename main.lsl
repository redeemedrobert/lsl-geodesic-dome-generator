//This is the script that goes in the main object that will serve as the root position of the geodesic dome to be generated.

float radius;
rez_triangle(vector pos, vector p1, vector p2, vector p3, integer i){
    //Pos is the position of the primitive doing the rezzing at the onset of dome calculation. p1 and p2 are the base and longest side of the triangle, p3 is the top point (or bottom if upside down). i is the position in rezzing order of the triangle currently being calculated; this determines a unique channel to communicate with the triangle on to send it size and shear information, so that there is no cross talk with another primitive.
    vector bmid = p1 + (p2-p1)/2; //middle point of the base of the triangle
    float a = llVecDist(p1,p3);
    float b = llVecDist(p1, p2);
    float c = llVecDist(p2, p3); //the distances between each point
    float s = (a+b+c)/2; //S in Heron formula
    float area = llSqrt(s*(s-a)*(s-b)*(s-c)); //Heron formula
    float h = area/(b/2); //Area = .5b*h solving for h
    float brtl = llSqrt(llPow(a,2) - llPow(h,2));//length of the base of right triangle of p1, p3, and p3-h
    float hb = b/2; //half of base
    float mrd = brtl - hb; //difference between the right triangles base length and half of the whole triangle's base
    vector brp = p1 + (bmid - p1)*(brtl/hb); //Position of the bottom right triangle's right angle point
    float shear = (brtl - hb)/b; //The difference between the base of right triangle and half of the actual base is the shear. Divided by base, we get the desired number
    vector fpos = bmid + (p3-brp)/2; //Bottom middle of triangle plus the difference between point 3 and bottom right tri point divided by two, gives us the exact middle of the triangle
    vector u = llVecNorm(fpos-bmid); //Normalized vector representing 1 meter above the center at its given rotation
    vector l = llVecNorm(p1-p2); //Normalized vector representing 1 meter to the left of the center at its given rotation
    rotation rot = llAxes2Rot(l%u, l, u); //Giving the Axes2Rot function forward, left and up, to produce the required orientation of the triangle. Forward is obtained by the modulus of left and up
    vector offset = <0.05,0,0> * rot; //Moving the triangle in 0.05m, as this is the thickness of the triangles. This creates a seemless dome
    if(p3.z < bmid.z) offset *= -1; //The triangles face outward if upside down, so the offset must be inverted to achieve the desired effect.
    fpos = (fpos + offset); //final position to render at
    llSetRegionPos(bmid); //There is a 10 meter rez distance, so move the primitive doing the rezzing to the position of the triangle bypass that limitation
    llRezObject("Triangle", fpos, ZERO_VECTOR, rot, i+1);
    llRegionSay(i+1+777, (string)<0.1,b,h> + "|" + (string)shear); //The triangle listens at (i+1+777) for [vector representing size]|[float representing shear] and sets each property to that upon receipt. The triangle's listener script then removes itself from the triangle's inventory to conserve simulator resources.
}

vector ppte(vector point, float radius, vector pos){ //push point to edge (of dome)
    float dist = llVecDist(point, pos); //3d distance between the point and the primitive doing the rezzing at the onset of dome calculation
    float diff = radius/dist; //This will either be 1.0 of the point is already [radius] away from the dome, or 1.01, 1.25, etcetera, in order to determine how much farther the point needs to go to get to the edge
    return pos + (point-pos)*diff; //The difference between point and pos is multiplied by difference and then added to pos to determine the final position of point, at the edge of the dome
}

set_triangle_freq(vector pos, vector p1, vector p2, vector p3, float radius, integer freq, integer ipos){ //Divide the triangle into smaller triangles based on frequency
    //Pos is position of rezzing primitie at onset of dome calculation, p1 to p2 is the base line of the dome, p3 is the top or bottom point depending on orientation, radius is the chosen radius, freq is the chosen frequency. The triangle will go from having 1 triangle to having each line divided by [freq], and then filling in the middle. ipos is the current triangle's position in the order of the initial 15 triangles that were generated
    float base_dist = llVecDist(p1, p2); //length of the base of the triangle
    vector bmid = p1 + ((p2-p1)/2); //Point in the middle of the base
    float adj_dist = llVecDist(p1, p3); //Length of the  p1 to p3
    float new_wid = base_dist/freq; //The new width of each triangle
    float new_hig = llVecDist(bmid,p3) / freq; //The new height of each triangle
    float new_adj_dist = adj_dist/freq; //The amount to move up and to the left in the triangle for each new row and each new p3
    list np1;
    list np2;
    list np3; //lists containing new p1-p3s.
    integer i;
    vector next_row_start = p1; //in the following for loop, this vector is adjust after each new row is generated, to begin calculating points on the next row starting at this point
    for(i=0; i<freq; ++i){
        integer ii;
        integer goal = freq-i; //Once the following for loop reaches this number, it will not generate another upside down triangle to the right of the right side up one, as this is the last triangle in the row
        vector next_column_start = next_row_start; //Same as next_row_start for the following for loop, except it is adjusted accordingly after each triangle is generated to move it to the next column over for veneration
        for(ii=0; ii<goal; ++ii){
            np1 += ppte(next_column_start, radius, pos); //p1 is the bottom left point, so it is exactly where the column begins
            np2 += ppte(next_column_start + (p2-p1)/freq, radius, pos); //add the new width, determined by frequency, to column_start to get the new bottom right point
            np3 += ppte(next_column_start + (p3 - p1)/freq, radius, pos);  //TThe difference between p3 and p1 divided by frequency is the new top point of each triangle
            //the ppte function pushes each point out to [radius] meters away from the center of the dome, to make the dome spherical
            if(ii!=goal-1){
                np1+= ppte(next_column_start + (p3 - p1)/freq, radius, pos);
                np2+= ppte((next_column_start + (p3-p1)/freq) + (p2-p1)/freq, radius, pos);
                np3 += ppte(next_column_start + (p2-p1)/freq, radius, pos);
                //Do all of the same, except with the goal of creating an upside down triangle between the previously created one and the next one, as long as the previously generated one is not the last column in this row
            }
            next_column_start = (next_column_start + (p2-p1)/freq); //move next column start over to begin generating new triangle(s, if another upside down triangle will be generated)
        }
        next_row_start = (next_row_start + (p3-p1)/freq); //Move to the next row up
    }
    for(i=0; i<llGetListLength(np1); i++){
        rez_triangle(pos, llList2Vector(np1, i), llList2Vector(np2, i), llList2Vector(np3,i), ipos*llGetListLength(np1) + i);
        //rez all triangles created by each point in the lists using the rez_triangle function above
    }
}

default{
    on_rez(integer s){
        llResetScript();
    }
    
    state_entry(){
        llListen(0, "", llGetOwner(), "");
    }
    
    listen(integer c, string s, key a, string m){
        if(llSubStringIndex(llToLower(m), "dome") == 0){ //The format of the message [m] must be dome [radius]:[frequency] or the message will be discarded
            radius = (float)llGetSubString(m, 5, llSubStringIndex(m,":")-1);
            integer freq = (integer)llGetSubString(m, llSubStringIndex(m, ":") + 1, -1);
            if(radius < 0) radius *= -1; //Why would someone choose a negative radius? I tried it. It made some very sad domes that were not domes at all :-(
            if(freq < 0) freq *= -1; //Same... I tried it. It doesn't do anything at all if you use a negative number.
            if(radius == 0) radius = 1; //That would not be a very aesthetically pleasing dome
            if(freq == 0) freq = 1; //A frequency of 0 will produce a division by zero error in the set_triangle_frequency function
            llOwnerSay("Generating a geodesic dome with a radius of " + (string)radius + " meters with a triangle frequency of " + (string)freq + ".");
            vector pos = llGetPos();
            list points;
            list point1; //point1 is the bottom left point in a triangle
            list point2; //point2 is bottom right
            list point3; //point3 is top
            vector top = pos + <0,0,radius>; //top of the dome
            float circ = TWO_PI * radius; //circumference of the dome, given [radius]
            integer i;
            for(i=0; i<5; ++i){
                points += pos + (<radius, 0, 0> * llEuler2Rot(<0,0, (360/5)*i> * DEG_TO_RAD));
                //5 points around the horizontal circumference of the dome, evenly spread
            }
            for(i=0; i<5; ++i){
                float side = llSqrt(llPow(radius,2)/2);
                vector offset = <side, 0, side> * llEuler2Rot(<0,0,(360/5*i)+(360/5/2)> * DEG_TO_RAD);
                points += pos + offset;
                //5 points in a horizontal circle on the dome halfway between the horizontal middle of the dome and the top of the dome
            }
            //Create 1V Triangles. This will be the dome if the frequency is set to 1, or they will be the templates for frequency increase otherwise
            point1 += llList2Vector(points,0); point2 += llList2Vector(points,1); point3 += llList2Vector(points,5);//base triangles pointing up
            point1 += llList2Vector(points,1); point2 += llList2Vector(points,2); point3 += llList2Vector(points,6);
            point1 += llList2Vector(points,2); point2 += llList2Vector(points,3); point3 += llList2Vector(points,7);
            point1 += llList2Vector(points,3); point2 += llList2Vector(points,4); point3 += llList2Vector(points,8);
            point1 += llList2Vector(points,4); point2 += llList2Vector(points,0); point3 += llList2Vector(points,9);
            point1 += llList2Vector(points,5); point2 += llList2Vector(points,6); point3 += llList2Vector(points,1);//base triangles pointing down
            point1 += llList2Vector(points,6); point2 += llList2Vector(points,7); point3 += llList2Vector(points,2);
            point1 += llList2Vector(points,7); point2 += llList2Vector(points,8); point3 += llList2Vector(points,3);
            point1 += llList2Vector(points,8); point2 += llList2Vector(points,9); point3 += llList2Vector(points,4);
            point1 += llList2Vector(points,9); point2 += llList2Vector(points,5); point3 += llList2Vector(points,0);
            point1 += llList2Vector(points,5); point2 += llList2Vector(points,6); point3 += top;//top level triangles
            point1 += llList2Vector(points,6); point2 += llList2Vector(points,7); point3 += top;
            point1 += llList2Vector(points,7); point2 += llList2Vector(points,8); point3 += top;
            point1 += llList2Vector(points,8); point2 += llList2Vector(points,9); point3 += top;
            point1 += llList2Vector(points,9); point2 += llList2Vector(points,5); point3 += top;
            
            //Rez Points
            //for(i=0; i<llGetListLength(points); i++){
            //    llRezObject("Point", llList2Vector(points, i), ZERO_VECTOR, ZERO_ROTATION, i+1);
            //} I used this loop early on to ensure that the points I was generating were what I wanted.
            
            //Rez triangles
            for(i=0; i<llGetListLength(point1); i++){ //unless there's been a catastrophic memory failure, point1 list should be the same length as the point2 and point3 lists
                vector p1 = llList2Vector(point1, i); //horizontal point 1
                vector p2 = llList2Vector(point2, i); //horizontal point 2
                vector p3 = llList2Vector(point3, i); //point opposite the horizontal side of the triangle
                set_triangle_freq(pos, p1, p2, p3, radius, freq, i); //Increases triangle frequency if freq is more than 1, otherwise, the same triangles are output
                /*vector bmid = p1 + (p2-p1)/2; //middle point of the horizontal side of the triangle
                vector vmid = bmid + (p3-bmid)/2; //point directly in the middle of all 3 points of the triangle
                float twidth = llVecDist(p1, p2); //width of the horizontal side of the triangle, will be the width of the tapered cube
                float theight = llVecDist(bmid, p3); //distance between the bmid point and the point opposite the horizontal side, will be the height of the tapered cube
                //It's about to get trigonometric in here
                float xrot;
                float yrot;
                float zrot;
                rotation frot;
                float opp = llVecDist(pos, <pos.x, bmid.y, pos.z>);
                float adj = llVecDist(bmid, <pos.x, bmid.y, bmid.z>);
                zrot = llAtan2(opp,adj) * RAD_TO_DEG;//Arctangent of a triangle where bmid across the hypotenuse from the center of the dome
                if(bmid.x>pos.x && bmid.y<pos.y) zrot = (360-zrot);
                if(bmid.x<pos.x && bmid.y<pos.y) zrot = (180 + zrot);
                if(bmid.x<pos.x && bmid.y>pos.y) zrot = (180 - zrot);
                frot = llEuler2Rot(<0,0,zrot> * DEG_TO_RAD);
                frot *= llEuler2Rot(<0,0,180> * DEG_TO_RAD); //the calculated rotation points positive x away, point it towards
                //Now calalculate for the y axis
                opp = llVecDist(p3, <p3.x,p3.y, bmid.z>);
                adj = llVecDist(bmid, <p3.x, p3.y, bmid.z>);
                yrot = 90 - (llAtan2(opp, adj) * RAD_TO_DEG);
                frot = (llEuler2Rot(<0,yrot,0> * DEG_TO_RAD) * frot);
                if(bmid.z > p3.z){ xrot = 180.0; frot = (llEuler2Rot(<xrot,0,0> * DEG_TO_RAD) * frot);}
                llSetRegionPos(vmid);
                llRezObject("Triangle", vmid, ZERO_VECTOR, frot, i+1);
                llRegionSay(i+1+777, (string)<.1, twidth, theight>);*/ //This was used early on to trigonometrically determine the required rotation for each triangle. This proved to not be an efficient method of determining rotation of the triangles once the triangles began to have top shear and base lines that were not horizontal because of higher triangle frequency, so I switched to llAxes2Rot instead. I left this here for nostalgic remembrance of the frustration that came with hours of trying to get this right when there was shear and non-horizontal base lines. The actual trigonometry was above in the rez_triangle function, but I chose not to keep it there because it was approaching (or greather than?) 100 lines of code and was very cluttered.
            }
            llSetRegionPos(pos); // Move back to the center of the dome after all triangle rezzing is complete
        }
    }
}
